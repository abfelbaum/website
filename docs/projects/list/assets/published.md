# Published

The project that hosts website assets under [assets.abfelbaum.dev](https://assets/abfelbaum.dev)

## Fonts

On a build the CI clones the [Fontsource GitHub project](https://github.com/fontsource/fontsource). Currently, the following fonts are published

| Name   | Path             |
|--------|------------------|
| Roboto | `/fonts/google/` |