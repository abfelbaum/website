---
sidebar_position: 2
---

# Philosophy

I try to follow the "Do one thing and do it well" and "KISS" principles in my projects. 

All of my projects are open source and licensed under one of the licenses explained [here](guidelines/license.md).