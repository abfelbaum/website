# License

Maybe I forgot to add a license to a project. My general license guidelines are:

* [AGPL v3](https://choosealicense.com/licenses/agpl-3.0/) for server projects (and everything that is not covered here)
* [GPL v3](https://choosealicense.com/licenses/gpl-3.0/) for client projects
* [LGPL v3](https://choosealicense.com/licenses/lgpl-3.0/) for libraries
* [MIT](https://choosealicense.com/licenses/mit/) for some projects where it is specified explicitly

:::info

A summary of each license is available when you follow the links

:::