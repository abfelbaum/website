# Project status

Every of my projects, that is not archived, is still maintained by me.

Most projects of mine do not have a continuous flow of issues to be worked on since they are fairly small projects. Due
to that fact there may have not been some commits or releases for a long time (AtAdminBot hasn't seen a new commit for a
year for example).

So, if a project is not archived, feel free to use it and to create issues.