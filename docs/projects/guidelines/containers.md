# Containers

I provide docker images for most of my projects. Here are some guidelines for these so you know what you're working
with:

The stable releases for every project will...
* ...be  rebuilt every 24 hours to consume security updates by their base images
* ...not be removed from the registry

The `latest` tag always points to the newest version.

All other tags will be removed after some unspecified amount of time.

Containers with prereleases can be recognized by alpha, beta or rc tags.

Stable releases:

* `1.0.0`
* `0.1.0`
* `10.2.5`

Prereleases:

* `1.0.0-beta`
* `0.1.0-alpha.35`
* `10.2.5-rc.2`