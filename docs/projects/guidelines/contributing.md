# Contributing

Everyone is welcome to contribute something to my projects.

## Creating issues

Do you have a suggestion for a new feature or did you find a bug? Please let me know and create an issue!

### with account

Everyone can log in to my GitLab instance via GitHub. When you are logged in you can create issues in the corresponding
projects.

There are some issue templates available. Please try to use and follow them. When no issue template fits your case, feel
free to just write anything.

### via email

When you do not want to log in via GitHub you're free to write an email. All projects have a GitLab ServiceDesk mail
that should be referenced in the corresponding readme.

If a project has no readme you can still contact me
via [this mail](mailto:reply+abfelbaum-website-38-issue-@git.abfelbaum.dev)

## Writing code

You are also welcome to contribute some code!

Since new users are not able to create projects you will need to notify me somehow. I would recommend to just create an
issue in a project or contact me via mail. I will be happy to help you out!

Please follow the overall style of the project you are contributing to so
it just fits in.