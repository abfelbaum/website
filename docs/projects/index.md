---
sidebar_position: 1
---

# Projects

Here you can find general information about my projects. 

:::note

All of my projects were made because I had a need for them. Therefore, they may just fit a specific use case (i.e. [AbfiBin](list/tools/abfibin.mdx) currently just supporting [ZITADEL](https://zitadel.com/) as an auth provider).

If you need them for a slightly different use case that is not too hard to implement please feel free to create [an issue](guidelines/contributing.md#creating-issues) in the corresponding project.

:::