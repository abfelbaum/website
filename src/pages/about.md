Software used by this website

* [Docusaurus](https://docusaurus.io/) ([MIT](https://opensource.org/licenses/MIT))
* [Fontawesome](https://fontawesome.com) ([CC BY 4.0 License](https://creativecommons.org/licenses/by/4.0/) / SIL OFL
  1.1 / [MIT](https://opensource.org/licenses/MIT))