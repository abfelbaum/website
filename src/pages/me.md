---
id: head-metadata
title: About me
---

<head>
  <meta name="robots" content="noindex, nofollow" />
</head>

# About me

The bloody facts: I am a 22 year old human being from germany that likes doing stuff with computers.

## Tech stuff

I really enjoy programming in C# and I am doing really everything in that language. Backends, CLIs, Desktop Software,
Web, like really everything. That's never getting boring.

I also enjoy (not that much, but I still enjoy it) working with my servers. Most of the stuff on that runs on a
HashiCorp Nomad cluster. I really love Hashicorp software and that is just, you won't believe that, because of their
configuration language (HCL). I don't know, it just feels really natural to read and write stuff in HCL.

I am documenting my infrastructure setup on this site too. You can have a look on that if you're curious!

## Non-tech stuff

I love driving with trains, buses, everything where I can just look out of the window and watch the landscape passing
by. It is best when it is raining, so you can also watch the raindrops running over the window. That is sooo relaxing!

When the weather is a bit better I like to cycle around, preferably with friends.

When I am at home I also like to cook and bake. When I am lazy I'll put myself in front of my computer and play either
some simulators (ETS 2, OMSI 2, FS), GTA, Minecraft or just sit there and watch something on YouTube or Netflix and
order some food, preferably Sushi.

I think I am finished for now, I really don't know what I should write here. Time will tell!