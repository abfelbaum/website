---
title: Welcome
description: Abfelbaums website. Provides documentation and more.
---

# Welcome

I am Abfelbaum and this is my website.

I mean, yes, it is very simple. The reason for that is that I am not a web designer. I didn't even bother with the
design and just took the default theme.. I know some HTML, CSS and
JavaScript, but that is not the stuff I am normally working with.

I have set up this website with [Docusaurus](https://docusaurus.io) so I can easily document my projects, infratructure
etc.
Maybe others or my future me can learn from that.

If you're interested about me, click [here](me).

Feel free to look around!