$structure = Get-Content structure.json | ConvertFrom-Json

# Make a new folder based upon a TempFileName
$tmpFolder = "$([convert]::tostring((get-random 65535), 16).padleft(4, '0') )";
Write-Information "Creating temporary folder $tmpFolder";
New-Item -ItemType Directory -Path $tmpFolder;


foreach ($project in $structure)
{
    $path = "$( $tmpFolder )/$( $project.name )";
    Write-Information "Pulling $( $project.name ) to $( $path )";
    git clone $project.url $path;

    if (Test-Path -Path $project.target)
    {
        Remove-Item -Recurse $project.target;
    }

    Write-Information "Creating $( $project.target )";
    New-Item -ItemType Directory -Path $project.target;
    Write-Information "Copy $( $project.name ) files";
    Copy-Item -Recurse "$( $path )/$( $project.path )/*" $project.target;

    Remove-Item -Recurse $path -Force;
}

Write-Information "Removing temporary folder $tmpFolder";
Remove-Item -Recurse $tmpFolder -Force;