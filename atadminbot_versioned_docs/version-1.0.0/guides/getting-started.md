---
description: "How to add the bot to your group."
---

# Getting started

## Prerequisites

You will need a [Telegram group](https://telegram.org/faq#q-how-do-i-create-a-group) where you can add the bot.

:::info

If you did not create the group please contact the creator or group administrators before adding the bot.

:::

## Add the bot to your group

To add the bot to a group follow [this link](https://t.me/atadminbot/startgroup) or just add the bot [like a regular group member](https://telegram.org/faq#q-how-do-i-add-more-members-what-39s-an-invite-link) (username: `@AtAdminBot`).

## Start the bot

You will need to [start the bot](usage/administrators.md#start-the-bot) to receive user reports. 