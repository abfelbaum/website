// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: 'Abfelbaum',
    tagline: 'Welcome to my website',
    url: process.env.DOCUSAURUS_URL,
    baseUrl: process.env.DOCUSAURUS_BASE_URL,
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'throw',
    // favicon: 'img/favicon.ico',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    // organizationName: 'facebook', // Usually your GitHub org/user name.
    // projectName: 'docusaurus', // Usually your repo name.

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: 'en',
        locales: ['en'],
    },

    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    id: 'default',
                    path: 'docs/projects',
                    routeBasePath: 'projects',
                    sidebarPath: 'docs/projects/sidebar.js'
                },
                blog: {
                    showReadingTime: true,
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            }),
        ],
    ],

    plugins: [
        ['@docusaurus/plugin-content-docs', {
            id: 'infrastructure',
            path: 'docs/infrastructure',
            routeBasePath: 'docs/infrastructure',
            sidebarPath: 'docs/infrastructure/sidebar.js',
        }],
        ['@docusaurus/plugin-content-docs', {
            id: 'abfibin',
            path: 'docs/tools/abfibin',
            routeBasePath: 'docs/tools/abfibin',
            sidebarPath: 'docs/tools/abfibin/sidebar.js'
        }],
        ['@docusaurus/plugin-content-docs', {
            id: 'atadminbot',
            path: 'docs/telegram/atadminbot',
            routeBasePath: 'docs/telegram/atadminbot',
            sidebarPath: 'docs/telegram/atadminbot/sidebar.js'
        }],
        ['@docusaurus/plugin-content-docs', {
            id: 'labelbot',
            path: 'docs/telegram/labelbot',
            routeBasePath: 'docs/telegram/labelbot',
            sidebarPath: 'docs/telegram/labelbot/sidebar.js'
        }],
        ['@docusaurus/plugin-content-docs', {
            id: 'whitelistbot',
            path: 'docs/telegram/whitelistbot',
            routeBasePath: 'docs/telegram/whitelistbot',
            sidebarPath: 'docs/telegram/whitelistbot/sidebar.js'
        }],
        ['@docusaurus/plugin-content-docs', {
            id: 'ci',
            path: 'docs/ci',
            routeBasePath: 'docs/ci',
            sidebarPath: 'docs/ci/sidebar.js'
        }],
        ['@docusaurus/plugin-ideal-image', {
            quality: 70,
            max: 1030, // max resized image's size.
            min: 640, // min resized image's size. if original is lower, use that size.
            steps: 2, // the max number of images generated between min and max (inclusive)
            disableInDev: false
        }],
        ['@docusaurus/plugin-pwa', {
            debug: true,
            offlineModeActivationStrategies: [
                'appInstalled',
                'standalone',
                'queryString',
            ],
            pwaHead: [
                {
                    tagName: 'link',
                    rel: 'icon',
                    href: '/img/docusaurus.png',
                },
                {
                    tagName: 'link',
                    rel: 'manifest',
                    href: '/manifest.json', // your PWA manifest
                },
                {
                    tagName: 'meta',
                    name: 'theme-color',
                    content: 'rgb(135, 64, 199)',
                },
                {
                    tagName: 'meta',
                    name: 'apple-mobile-web-app-capable',
                    content: 'yes',
                },
                {
                    tagName: 'meta',
                    name: 'apple-mobile-web-app-status-bar-style',
                    content: '#000',
                },
                {
                    tagName: 'link',
                    rel: 'apple-touch-icon',
                    href: '/img/docusaurus.png',
                },
                {
                    tagName: 'link',
                    rel: 'mask-icon',
                    href: '/img/docusaurus.svg',
                    color: 'rgb(135, 64, 199)',
                },
                {
                    tagName: 'meta',
                    name: 'msapplication-TileImage',
                    content: '/img/docusaurus.png',
                },
                {
                    tagName: 'meta',
                    name: 'msapplication-TileColor',
                    content: '#000',
                },
            ]
        }],
    ],

    themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
            navbar: {
                title: 'Abfelbaum',
                items: [
                    {
                        type: 'dropdown',
                        label: 'Projects',
                        to: '/projects',
                        items: [
                            {
                                type: 'html',
                                value: '<b>Tools</b>'
                            },
                            {
                                type: 'doc',
                                label: 'AbfiBin',
                                docId: 'index',
                                docsPluginId: 'abfibin'
                            },
                            {
                                type: 'html',
                                value: '<b>Telegram Bots</b>'
                            },
                            {
                                type: 'doc',
                                label: 'AtAdminBot',
                                docId: 'index',
                                docsPluginId: 'atadminbot'
                            },
                            {
                                type: 'doc',
                                label: 'LabelBot',
                                docId: 'index',
                                docsPluginId: 'labelbot'
                            },
                            {
                                type: 'doc',
                                label: 'WhitelistBot',
                                docId: 'index',
                                docsPluginId: 'whitelistbot'
                            },
                        ]
                    },
                    {
                        type: 'dropdown',
                        label: 'Infrastructure',
                        to: '/docs/infrastructure',
                        items: [
                            {
                                type: 'doc',
                                label: 'CI/CD',
                                docId: 'index',
                                docsPluginId: 'ci'
                            },
                        ]
                    },
                    {
                        type: 'docsVersionDropdown',
                        position: 'right',
                        docsPluginId: 'abfibin'
                    }
                ],
            },
            footer: {
                style: 'dark',
                links: [
                    {
                        title: 'Tools',
                        items: [
                            {
                                label: 'AbfiBin',
                                to: '/docs/tools/abfibin',
                            },
                        ],
                    },
                    {
                        title: 'Bots',
                        items: [
                            {
                                label: 'AtAdminBot',
                                to: '/docs/telegram/atadminbot'
                            },
                            {
                                label: 'LabelBot',
                                to: '/docs/telegram/labelbot'
                            },
                            {
                                label: 'WhitelistBot',
                                to: '/docs/telegram/whitelistbot'
                            }
                        ],
                    },
                    {
                        title: 'More',
                        items: [
                            {
                                label: 'About this website',
                                href: '/about'
                            },
                            {
                                label: 'Blog',
                                to: '/blog'
                            },
                            {
                                label: 'Contact',
                                href: 'mailto:reply+abfelbaum-website-38-issue-@git.abfelbaum.dev'
                            },
                            {
                                label: 'Source',
                                href: 'https://git.abfelbaum.dev/abfelbaum/website'
                            },
                        ],
                    },
                ],
                copyright: `Licensed under AGPLv3 by Abfelbaum, ${new Date().getFullYear()}`,
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme,
                additionalLanguages: ['csharp', 'hcl']
            },
        }),
    themes: [
        // ... Your other themes.
        [
            require.resolve("@easyops-cn/docusaurus-search-local"),
            /** @type {import("@easyops-cn/docusaurus-search-local").PluginOptions} */
            ({
                // ... Your options.
                // `hashed` is recommended as long-term-cache of index file is possible.
                hashed: true,
                // For Docs using Chinese, The `language` is recommended to set to:
                // ```
                // language: ["en", "zh"],
                // ```
                indexPages: true,
                removeDefaultStopWordFilter: true,
                removeDefaultStemmer: true,
                highlightSearchTermsOnTargetPage: true
            }),
        ],
    ],
};

module.exports = config;
